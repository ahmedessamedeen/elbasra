package elbasra.scoptech.com.sync;

import android.app.ProgressDialog;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import elbasra.scoptech.com.helpers.AppController;

/**
 * Created by Darsh on 08/11/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private static final String FBToken_SERVICE = "http://demos.magdsoft.com/basra2/setFBApiToken?fb_token=";
    static String refreshedToken;
    @Override
    public void onTokenRefresh() {

        // Getting registration token
         refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Displaying token on logcat
        // sony
        // fl7cQDxl2dc:APA91bEJhXMJIXKLEp6VTbEYWu8NOp-kCziT5ESxE6dAmi3CymRCCATMpEzfVeUCaiwrq4aWV7WcsKFJRjtvK-WM8d-ZzSf1TRHeES1I2AsYoslu7MHdXvBkvW9eIpqVYI2upvVtKGoW
        // lenovo
        // d_U0APg8Nys:APA91bEQkTN7vCJgKIrJ1KTi5Gk1vn8EwdpH-G9wKDNGluKuuGGQHDX6-E-3FbZ8nVWz7zhS89zsAsPe2TPaeqPj8Q06O4hw1vEp3mwqXVzTip_afjegnEURycvdtwYz-sMZvakr9HWU



        Log.d(TAG, "Refreshed token : " + refreshedToken);

        FirebaseMessaging.getInstance().subscribeToTopic("content");

        setApiToken();
    }

    private void sendRegistrationToServer(String token) {
         //You can implement this method to store the token on your server
        //Not required for current project
    }
    private void setApiToken() {
        JsonObjectRequest jsonObjReq = null;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                FBToken_SERVICE + refreshedToken +"&device=android", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("", response.toString());
                //data_sideList.clear();
                try {
                    // Parsing json object response
                    // response will be a json object
                    String status = response.getString("status");
                    Log.e("notification response",response.toString());
                    Log.e("sent api",refreshedToken);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("loading", "done");
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("", "Error: " + error.getMessage());

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

}
