package elbasra.scoptech.com.sync;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.acitivities.OneNewsNotification;
import elbasra.scoptech.com.acitivities.OneVideoNotification;


/**
 * Created by Darsh on 08/11/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService{

    private static final String TAG = "MyFirebaseMsgService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Displaying data in log
        Log.d(TAG , "From : " + remoteMessage.getFrom());
//        Log.d(TAG , "Notification Message Body : " + remoteMessage.getNotification().getBody());

        // calling Method to generate notification
//        Arrays.deepToString()
//        Log.e("TheMap", remoteMessage.getData().toString());
//        Log.e("TheMap", Arrays.deepToString(remoteMessage.getData().entrySet().toArray()));
//        RemoteMessage remoteMessage1 = remoteMessage.getData();
        sendNotification(remoteMessage.getData());
    }

    // this Method is only generating push notification
    private void sendNotification(Map<String, String> messageBody){
        String commenterName="" , link = "";
        //  if you want return your notification new one in device bar than use this randomID
        Random random = new Random();
        int randomID = random.nextInt(9999 - 1000) + 1000;
        int id = 0 , type = 0 ;

        try {
            JSONObject body = new JSONObject(messageBody.get("body"));
            id = body.getInt("id");
            type = body.getInt("type");
            link = getYouTubeId(body.getString("link"));
            commenterName = body.getString("title");
            /*JSONObject adobj = ads.getJSONObject(0);
            adTitle = adobj.getString("name");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent;
        if(type == 5){
            intent = new Intent(this , OneVideoNotification.class);
            intent.putExtra("youtube_id", Integer.toString(id));
            intent.putExtra("url" , link);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }else{
            intent = new Intent(this , OneNewsNotification.class);
            intent.putExtra("newsid", id);
            intent.putExtra("type", type);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION); // this sound for deafultsound
//        Uri defaultSoundUri = Uri.parse("android.resource://com.magd.sendnotificationfcm/" + R.raw.ta_da); // this sound for custom sound
        long[] vibrate = { 0, 100, 200, 300 };

        Log.e("message body ", messageBody.toString());
        if(null == commenterName || commenterName.isEmpty()){
            commenterName = messageBody.get("title");
        }
//        userImage = messageBody.get("user_image");
        // {myPropertyId=11, body=العنوان, sound=1, title=تم أضافة خبر جديد, vibrate=1}
        //{myPropertyId=11, body={"id":116,"title":"تم أضافة  خبر جديد","type":"1"}, sound=1, title=اختبار التحديث الجديد 844, vibrate=1}
        NotificationCompat.Builder notif = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_basra_icon)
                .setLargeIcon(BitmapFactory.decodeResource(Resources.getSystem(),R.drawable.ic_basra_icon))
                .setContentTitle("البصرة الاخبارى")
                .setContentText(commenterName)
                .setColor(ContextCompat.getColor(this,R.color.colorAccent))
                .setAutoCancel(true)
                .setVibrate(vibrate)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 , notif.build());}

    public static Bitmap getBitmapFromURL(String src) {

        /*Drawable d = ContextCompat.getDrawable(null,R.drawable.icon);
        Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
        Bitmap bitmap1 = BitmapFactory.decodeResource(Resources.getSystem(),R.drawable.icon);*/

        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getYouTubeId (String youTubeUrl) {
        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if(matcher.find()){
            return matcher.group();
        } else {
            return "error";
        }
    }
}
