package elbasra.scoptech.com.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.acitivities.OneNews;
import elbasra.scoptech.com.models.getData;

/**
 * Created by m on 31/12/2016.
 */

public class SearchAdapter extends RecyclerView.Adapter<RecHolder>{
    Context context;
    ArrayList<getData> listData = new ArrayList<>();

    public SearchAdapter(Context context, ArrayList<getData> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_list_item , parent , false);
        return new RecHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecHolder holder, final int position) {
        holder.title.setText(listData.get(position).getSearchTitle());
        holder.since.setText(listData.get(position).getSearchSince());
        holder.views.setText(Integer.toString(listData.get(position).getSearchViews()));
        Glide.with(context)
                .load(listData.get(position).getSearchImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progress.setVisibility(View.GONE);
                        return false;
                    }
                }).into(holder.image);
        holder.ll_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goOneNews = new Intent(context , OneNews.class);
                goOneNews.putExtra("newsid" , listData.get(position).getSearchNewsId());
                goOneNews.putExtra("typeid" , listData.get(position).getSearchType());
                context.startActivity(goOneNews);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}

