package elbasra.scoptech.com.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Adapter;
import android.widget.TextView;
import android.widget.Toast;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.helpers.CheckInternet;

/**
 * Created by m on 26/12/2016.
 */
class ClickLoadMoreHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView tv_click;
    RecyclerView.Adapter<RecyclerView.ViewHolder> parent;

    public ClickLoadMoreHolder(View itemView, RecyclerView.Adapter<RecyclerView.ViewHolder> parent) {
        super(itemView);
        this.parent = parent;
        tv_click = (TextView) itemView.findViewById(R.id.tv_loadmore);
        tv_click.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (CheckInternet.internetConnectionAvailable(3000)) {
            if (parent instanceof RecAdapter) {
                RecAdapter adapter = (RecAdapter) parent;
                adapter.isButton = false;
                adapter.notifyDataSetChanged();
            } else if (parent instanceof CommentAdapter) {
                CommentAdapter adapter = (CommentAdapter) parent;
                adapter.isButton = false;
                adapter.notifyDataSetChanged();
            } else if (parent instanceof VideosAdapter) {
                VideosAdapter adapter = (VideosAdapter) parent;
                adapter.isButton = false;
                adapter.notifyDataSetChanged();
            }
        } else {
            if (parent instanceof RecAdapter) {
                RecAdapter adapter = (RecAdapter) parent;
                Toast.makeText(adapter.context, adapter.context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            } else if (parent instanceof CommentAdapter) {
                CommentAdapter adapter = (CommentAdapter) parent;
                Toast.makeText(adapter.context, adapter.context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            } else if (parent instanceof VideosAdapter) {
                VideosAdapter adapter = (VideosAdapter) parent;
                Toast.makeText(adapter.context, adapter.context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }

        }
    }
}
