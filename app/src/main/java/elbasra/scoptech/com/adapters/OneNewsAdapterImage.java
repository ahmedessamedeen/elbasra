package elbasra.scoptech.com.adapters;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.acitivities.OneNews;
import elbasra.scoptech.com.models.getData;


/**
 * Created by macpro on 11/9/16.
 */

public class OneNewsAdapterImage extends BaseAdapter implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    private Context context;
    public static ArrayList<getData> mDataset;
    public ViewPager viewPager;

    public OneNewsAdapterImage(Context c, ArrayList<getData> myDataset) {
        context = c;
        this.mDataset=myDataset;
    }
    public int getCount() {
        return mDataset.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, final View convertView, ViewGroup parent) {


        View gridView;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        gridView = (View) inflater.inflate(R.layout.one_single_image, null);
        viewPager = (ViewPager) gridView.findViewById(R.id.pager);
        viewPager.setAdapter(new CustomPagerAdapter(context,mDataset));

        CirclePageIndicator titleIndicator = (CirclePageIndicator) gridView.findViewById(R.id.titles);
        titleIndicator.setViewPager(viewPager);


        return gridView;


    }


    //    -=-=-=-=-=- -=-=-=-=-=- get main products -=-=-=-=-=- -=-=-=-=-=- -=-=-=-=-=-




    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
