package elbasra.scoptech.com.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.acitivities.News;
import elbasra.scoptech.com.acitivities.OneNews;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.helpers.CheckInternet;
import elbasra.scoptech.com.models.getData;

/**
 * Created by m on 25/12/2016.
 */




public class RecAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<getData> listData = new ArrayList<>();
    View itemView;
    int VIEW_TYPE_ITEM = 0;
    int VIEW_LOAD_ITEM = 1;
    int VIEW_CLICK_ITEM = 2;
    boolean isButton = true;
    boolean loadMore = true;
    boolean enough = true;
    int page = 1;
    public static int NewsID;

    public RecAdapter(Context context, ArrayList<getData> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_list_item, parent, false);
            return new RecHolder(itemView);
        } else if (viewType == VIEW_LOAD_ITEM) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false);
            return new ProgressBarHolder(itemView);
        } else if (viewType == VIEW_CLICK_ITEM) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_click_item, parent, false);
            return new ClickLoadMoreHolder(itemView, this);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof RecHolder) {
            final RecHolder recHolder = (RecHolder) holder;
            recHolder.title.setText(listData.get(position).getTitle());
            recHolder.since.setText(listData.get(position).getSince());
            recHolder.views.setText(Integer.toString(listData.get(position).getViews()));
            recHolder.progress.setIndeterminate(true);
            Glide.with(context)

                    .load(listData.get(position).getImage())
                    .error(R.drawable.image_holder2)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            recHolder.progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            recHolder.progress.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(recHolder.image);
//            }


            ((RecHolder) holder).ll_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goOneNews = new Intent(context, OneNews.class);
                    goOneNews.putExtra("newsid", listData.get(position).getNewsId());
                    goOneNews.putExtra("type", listData.get(position).getType());

                    goOneNews.putExtra("news",listData.get(position));
                    context.startActivity(goOneNews);
                }
            });
        } else if (holder instanceof ProgressBarHolder) {
            ProgressBarHolder recLoadHolder = (ProgressBarHolder) holder;
            if (CheckInternet.internetConnectionAvailable(3000)) {
                if (enough) {
                    RecVolley(News.ALL_NEWS_URL2 + Integer.toString(News.TypeID) + "&page=" + Integer.toString(page));
                    recLoadHolder.progressBar.setIndeterminate(true);
                } else {
                    recLoadHolder.progressBar.setVisibility(View.GONE);
                }
            } else {
                Toast.makeText(context, context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public int getItemCount() {
        if (page == 1) {
            return listData.size() + 1;
        }
        return listData.size() + (loadMore ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (page == 1) {
            return position >= listData.size() ? VIEW_LOAD_ITEM : VIEW_TYPE_ITEM;
        }
        return position >= listData.size() ? (isButton ? VIEW_CLICK_ITEM : VIEW_LOAD_ITEM) : VIEW_TYPE_ITEM;
    }

    public void RecVolley(String url) {
        final String TAG = "your Data Return is :";
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
                try {
                    String status = response.getString("Status");
                    JSONArray dataArray = response.getJSONArray("Data");
                    if (status.equals("Done")) {
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            getData getData = new getData();
                            getData.setNewsId(dataObject.getInt("id"));
                            NewsID = dataObject.getInt("id");
                            getData.setTitle(dataObject.getString("name"));
                            getData.setContent(dataObject.getString("content"));
                            getData.setImage(dataObject.getString("image"));
                            getData.setSince(dataObject.getString("date"));
                            getData.setViews(dataObject.getInt("views"));
                            getData.setType(dataObject.getInt("type"));
                            JSONArray images = dataObject.getJSONArray("imageURLs");
                            String [] myimages = new String[images.length()];
                            for (int z = 0;z<images.length();z++){
                                JSONObject object = images.getJSONObject(z);
                                myimages[z] =object.getString("image");
                            }
                            getData.setImages(myimages);
                            listData.add(getData);
                        }
                        page++;
                        isButton = true;
                        enough = true;
                        notifyDataSetChanged();
                        if (listData == null | listData.size() <= 9) {
                            loadMore = false;
                        }
                    } else {
                        enough = false;
                        notifyDataSetChanged();
                        Toast.makeText(context, context.getString(R.string.no_news), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, context.getString(R.string.error_server), Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }


}
class RecHolder extends RecyclerView.ViewHolder {
    TextView title, since, views;
    ImageView image;
    LinearLayout ll_click;
    ProgressBar progress;

    public RecHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.tv_title);
        since = (TextView) itemView.findViewById(R.id.tv_since);
        views = (TextView) itemView.findViewById(R.id.tv_views);
        image = (ImageView) itemView.findViewById(R.id.iv_image);
        ll_click = (LinearLayout) itemView.findViewById(R.id.ll_click);
        progress = (ProgressBar) itemView.findViewById(R.id.progressBar);
    }
}
