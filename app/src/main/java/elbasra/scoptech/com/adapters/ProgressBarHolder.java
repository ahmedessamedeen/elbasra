package elbasra.scoptech.com.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import elbasra.scoptech.com.R;

/**
 * Created by m on 26/12/2016.
 */

public class ProgressBarHolder extends RecyclerView.ViewHolder {
    public ProgressBar progressBar;
    public ProgressBarHolder(View itemView) {
        super(itemView);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
    }
}
