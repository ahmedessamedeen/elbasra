package elbasra.scoptech.com.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.models.getData;


/**
 * Created by m on 27/12/2016.
 */

public class NavListAdapter extends ArrayAdapter<getData> {
    Context context;
    int resLayout;
    List<getData> listData;
    public NavListAdapter(Context context, int resource, List<getData> listData) {
        super(context, resource, listData);
        this.context = context;
        this.resLayout = resource;
        this.listData = listData;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = View.inflate(context , resLayout , null);
        TextView tv_title = (TextView) v.findViewById(R.id.tv_nav_title);
        ImageView iv_icon = (ImageView) v.findViewById(R.id.iv_nav_icon);

        getData getData = listData.get(position);
        tv_title.setText(getData.getNavTitle());
        iv_icon.setImageResource(getData.getNavIcon());
        return v;

    }
}
