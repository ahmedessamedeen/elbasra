package elbasra.scoptech.com.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.acitivities.News;
import elbasra.scoptech.com.acitivities.OneNews;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.helpers.CheckInternet;
import elbasra.scoptech.com.models.getData;

/**
 * Created by m on 25/12/2016.
 */

class CommentHolder extends RecyclerView.ViewHolder {

    public TextView tv_comment;

    public CommentHolder(View itemView) {
        super(itemView);
        tv_comment = (TextView) itemView.findViewById(R.id.tv_comment);
    }
}

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<getData> dataList = new ArrayList<>();
    View itemView;
    int VIEW_TYPE_ITEM = 0;
    int VIEW_LOAD_ITEM = 1;
    int VIEW_CLICK_ITEM = 2;
    boolean isButton = true;
    boolean loadMore = true;
    int page = 1;
    String url;

    public CommentAdapter(Context context, String url) {
        this.context = context;
        this.url = url;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_comment_item, parent, false);
            return new CommentHolder(itemView);
        } else if (viewType == VIEW_LOAD_ITEM) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false);
            return new ProgressBarHolder(itemView);
        } else if (viewType == VIEW_CLICK_ITEM) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_click_item, parent, false);
            return new ClickLoadMoreHolder(itemView, this);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CommentHolder) {
            CommentHolder holderComment = (CommentHolder) holder;
            holderComment.tv_comment.setText(dataList.get(position).getComment());
        } else if (holder instanceof ProgressBarHolder) {
            ProgressBarHolder holderLoad = (ProgressBarHolder) holder;
            if (CheckInternet.internetConnectionAvailable(3000)) {
                // here your Volley Task
                OneNewsVolley(url + "&page=" + page);
            } else {
                Toast.makeText(context, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
            }
            holderLoad.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        if (page == 1) {
            return dataList.size() + 1;
        }
        return dataList.size() + (loadMore ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (page == 1) {
            return position >= dataList.size() ? VIEW_LOAD_ITEM : VIEW_TYPE_ITEM;
        }
        return position >= dataList.size() ? (isButton ? VIEW_CLICK_ITEM : VIEW_LOAD_ITEM) : VIEW_TYPE_ITEM;
    }

    public void OneNewsVolley(String url) {
        final String TAG = "your Response Comment is :";
        Log.i("your comments url is :", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
                try {

                    String status = response.getString("Status");
                    if (status.equals("Done")) {
                        JSONArray commentArray = null;
                        try{
                            JSONObject dataObject = response.getJSONObject("0");
                            commentArray = dataObject.getJSONArray("comments");
                        }catch (Exception e){}
                        try{
                            JSONArray Data = response.getJSONArray("Data");
                            JSONObject DataArray = Data.getJSONObject(0);
                            commentArray = DataArray.getJSONArray("comments");
                        }catch (Exception e){}
                        if (commentArray.length() != 0) {
                            for (int i = 0; i < commentArray.length(); i++) {
                                getData getData = new getData();
                                JSONObject commentObject = commentArray.getJSONObject(i);
                                getData.setComment(commentObject.getString("comment"));
                                dataList.add(getData);
                            }
                            page++;
                            isButton = true;
                            notifyDataSetChanged();

                        } else {
                            page = 0;
                            loadMore = false;
//                            Toast.makeText(context, context.getString(R.string.no_comment), Toast.LENGTH_SHORT).show();
                            notifyDataSetChanged();
                        }

                        if ((dataList == null) || (dataList.size() == 0)) {
                            loadMore = false;
                        }
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, context.getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}
