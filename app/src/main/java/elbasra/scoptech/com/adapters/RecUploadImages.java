package elbasra.scoptech.com.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.acitivities.SendNews;
import elbasra.scoptech.com.interfaces.ItemTouchHelperAdapter;
import elbasra.scoptech.com.interfaces.ItemTouchHelperViewHolder;
import elbasra.scoptech.com.models.getData;


/**
 * Created by Darsh on 30/11/2016.
 */

class RecUploadImagesHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
    public ImageView image;


    public RecUploadImagesHolder(View itemView) {
        super(itemView);
        image = (ImageView) itemView.findViewById(R.id.iv_image);

    }
    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }
}

public class RecUploadImages extends RecyclerView.Adapter<RecUploadImagesHolder>
        implements ItemTouchHelperAdapter {
    Context context;
    List<getData> listImage = new ArrayList<getData>();

    public RecUploadImages(Context context, List<getData> listImage) {
        this.context = context;
        this.listImage = listImage;
    }

    @Override
    public RecUploadImagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.custom_recycle_upload_item , parent , false);
        return new RecUploadImagesHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecUploadImagesHolder holder, final int position) {
        try {
            holder.image.setImageBitmap(BitmapFactory.decodeFile((SendNews.image_path.get(position))));

           /* holder.setItemClickListener(new RecItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    AddAdvert.image_path.remove(position);
                    listImage.remove(position);
                    notifyItemRemoved(position);
                }
            });*/
        }catch (Exception e){

        }


    }

    @Override
    public int getItemCount() {
        return listImage.size();
    }

    @Override
    public void onItemDismiss(int position) {
        SendNews.image_path.remove(position);
        listImage.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(listImage, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

}
