package elbasra.scoptech.com.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.acitivities.YoutubePlayer;
import elbasra.scoptech.com.models.Video;

/**
 * Created by m on 01/01/2017.
 */

public class SearchVideosAdapter extends RecyclerView.Adapter<VideosAdapterHolder> {
    Context context;
    ArrayList<Video> dataList = new ArrayList<>();

    public SearchVideosAdapter(Context context, ArrayList<Video> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public VideosAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_youtube_video, parent, false);
        return new VideosAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(final VideosAdapterHolder holder, final int position) {
        try {
            holder.title.setText(dataList.get(position).getTitle());
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, YoutubePlayer.class)
                            .putExtra("title", dataList.get(position).getTitle())
                            .putExtra("url", dataList.get(position).getUrl())
                            .putExtra("youtube_id", dataList.get(position).getYoutube_id()));
                }
            });
            holder.views.setText(Integer.toString(dataList.get(position).getViews()));
            holder.since.setText(dataList.get(position).getDate());
            Log.i("image url is", "http://img.youtube.com/vi/" + dataList.get(position).getYoutube_id() + "/0.jpg");
            Glide.with(context)
                    .load("http://img.youtube.com/vi/" + dataList.get(position).getYoutube_id() + "/0.jpg")
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.progress.setVisibility(View.GONE);
                            holder.over_image.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.progress.setVisibility(View.GONE);
                            holder.over_image.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(holder.image);
        } catch (Exception e) {
            Log.e("erro is :", e.toString());
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
