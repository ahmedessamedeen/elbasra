package elbasra.scoptech.com.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.acitivities.YoutubePlayer;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.helpers.CheckInternet;
import elbasra.scoptech.com.models.Video;

/**
 * Created by ahmed on 26/12/16.
 */

class VideosAdapterHolder extends RecyclerView.ViewHolder{
    TextView title,views,since;
    ImageView image,over_image;
    ProgressBar progress;

    public VideosAdapterHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.tv_video_title);
        views = (TextView) itemView.findViewById(R.id.tv_views);
        since = (TextView) itemView.findViewById(R.id.tv_since);
        image = (ImageView) itemView.findViewById(R.id.img_thumnail);
        over_image = (ImageView)itemView.findViewById(R.id.img_over);
        progress = (ProgressBar) itemView.findViewById(R.id.progressBar);

    }
}

public class VideosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<Video> dataList = new ArrayList<>();
    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_LOAD_ITEM = 1;
    private static final int VIEW_CLICK_ITEM = 2;
    private static final int TYPE_HEADER = 3;
    boolean isButton = true;
    boolean loadMore = true;
    boolean enough = true;
    int page = 1;
    String search_text;
    public VideosAdapter(Context context, ArrayList<Video> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_youtube_video, parent, false);
            return new VideosAdapterHolder(view);
        }else if (viewType == VIEW_LOAD_ITEM){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false);
            return new ProgressBarHolder(view);
        }else if (viewType == VIEW_CLICK_ITEM){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_click_item, parent, false);
            return new ClickLoadMoreHolder(view , this);
        }else if (viewType == TYPE_HEADER) {
            final View view = LayoutInflater.from(context).inflate(R.layout.recycle_header, parent, false);
            return new VideosAdapterHolder(view);
        }
        throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types    correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof VideosAdapterHolder&&!isPositionHeader(position)){
            final VideosAdapterHolder holder1 = (VideosAdapterHolder) holder;
            try {
                holder1.title.setText(dataList.get(position-1).getTitle());
                holder1.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, YoutubePlayer.class)
                                .putExtra("title", dataList.get(position-1).getTitle())
                                .putExtra("url", dataList.get(position-1).getUrl())
                                .putExtra("youtube_id", dataList.get(position-1).getYoutube_id())
                                .putExtra("id", ""+dataList.get(position-1).getId()));

                    }
                });
                Log.e(" data with error 1",Integer.toString(dataList.get(position-1).getViews()));
                Log.e(" data with error 2",dataList.get(position-1).getDate());

                holder1.views.setText(Integer.toString(dataList.get(position-1).getViews()));
                holder1.since.setText(dataList.get(position-1).getDate());


                Log.i("image url is", "http://img.youtube.com/vi/" + dataList.get(position-1).getYoutube_id() + "/0.jpg");
                Glide.with(context)
                        .load("http://img.youtube.com/vi/" + dataList.get(position-1).getYoutube_id() + "/0.jpg")
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                holder1.progress.setVisibility(View.GONE);
                                holder1.over_image.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                holder1.progress.setVisibility(View.GONE);
                                holder1.over_image.setVisibility(View.VISIBLE);
                                return false;
                            }
                        })
                        .into(holder1.image);
            }catch (Exception e){
                Log.e("erro is :" , e.toString());
            }


        }else if(holder instanceof ProgressBarHolder){
            ProgressBarHolder holder1 = (ProgressBarHolder) holder;
            if(CheckInternet.internetConnectionAvailable(3000)){
                // volley task
                if(enough){
                    YoutubeVolley("http://basra.magdsoft.com/searchVideo?page=" + page);
                }else {
                    holder1.progressBar.setVisibility(View.GONE);
                }
            }else{
            }
            holder1.progressBar.setIndeterminate(true);
        }


    }

    @Override
    public int getItemViewType(int position) {
        if(page == 1) {
            return position > dataList.size() ? VIEW_LOAD_ITEM : (isPositionHeader(position)?TYPE_HEADER:VIEW_TYPE_ITEM);
        }
//        else if(isPositionHeader(position)){
//            return TYPE_HEADER;
//        }
        return position > dataList.size() ?(isButton ? VIEW_CLICK_ITEM:VIEW_LOAD_ITEM)  :(isPositionHeader(position)?TYPE_HEADER:VIEW_TYPE_ITEM);
    }

    @Override
    public int getItemCount() {

        return dataList.size() +1+(loadMore ? 1:0);
    }

    private String getYouTubeId (String youTubeUrl) {
        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if(matcher.find()){
            return matcher.group();
        } else {
            return "error";
        }
    }

    public void YoutubeVolley(String url){
        final String TAG = "your Data Return :";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG , response.toString());
                try {
                    String status = response.getString("Status");
                    JSONArray dataArray = response.getJSONArray("Data");
                    if(status.equals("Done")) {
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            Video video = new Video(dataObject.getInt("id"),dataObject.getString("name"),dataObject.getString("link"),dataObject.getInt("views"),getYouTubeId(dataObject.getString("link")),dataObject.getString("date"));
                            dataList.add(video);
                            Log.i("video object id: ",""+dataObject.getInt("id"));
                        }
                        page++;
                        isButton = true;
                        enough = true;
                        notifyDataSetChanged();
                        if(dataList == null | dataList.size() <= 9){
                            loadMore = false;
                        }
                    }else{
                        enough = false;
                        notifyDataSetChanged();
                        Toast.makeText(context, context.getString(R.string.no_videos), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, context.getString(R.string.error_loading), Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
    private boolean isPositionHeader(int position) {
        return position == 0;
    }
}
