package elbasra.scoptech.com.adapters;

/**
 * Created by ahmed on 9/29/2016.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.acitivities.OneImageShow;
import elbasra.scoptech.com.acitivities.OneNews;
import elbasra.scoptech.com.helpers.CheckInternet;
import elbasra.scoptech.com.models.getData;

public class CustomPagerAdapter extends PagerAdapter {

    private Context mContext;
    public ArrayList<getData> mDataset;
    public  String[]  ProductImagesShow ;
    ProgressBar progressBar;

    public CustomPagerAdapter(Context context, ArrayList<getData> myDataset) {
        this.mContext = context;
        this.mDataset=myDataset;
    }


    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.one_image, collection, false);
        final ImageView productImage = (ImageView) layout.findViewById(R.id.image);
        progressBar = (ProgressBar) layout.findViewById(R.id.login_progress);
        Glide.with(mContext)
                .load(mDataset.get(position).getOneImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(productImage);

        collection.addView(layout);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckInternet.internetConnectionAvailable(3000)) {
                    Intent intent = new Intent(mContext,OneImageShow.class);
                    intent.putExtra("image", mDataset.get(position).getOneImage());
                    mContext.startActivity(intent);
                } else {
                    Toast.makeText(mContext,mContext.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }

            }
        });

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        ProductImagesShow = new String[mDataset.size()];
        for (int i = 0 ; i < ProductImagesShow.length ; i++){
            ProductImagesShow[i] = mDataset.get(i).getOneImage();
        }
        return mDataset.size();

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }





}
