package elbasra.scoptech.com.models;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by m on 25/12/2016.
 */


/***
 * Class for getting data
 */

public class getData implements Serializable {
    String[] Images;
    int NewsId;
    String Title;
    String OneImage;
    int Type;
    int Views;
    String Image;
    String Since;
    String Comment;
    String Content;
    ImageView imageview;
    String ImgsURL[];
    String SearchTitle;
    int SearchViews;
    String SearchImage;
    String SearchSince;
    int SearchType;
    int SearchNewsId;
    String OneImageShow;

    /**
     * this for Navigation Item List
     */
    String NavTitle;
    int NavIcon;

    public getData() {
    }

    /**
     * Gets data from
     *
     * @param navTitle The navigation bar title
     * @param navIcon  The navigation bar icon
     */
    public getData(String navTitle, int navIcon) {
        NavTitle = navTitle;
        NavIcon = navIcon;
    }


    public getData(ImageView image) {
        this.imageview = image;
    }

    public ImageView getImageview() {
        return imageview;
    }

    public getData(String[] imgsURL) {
        ImgsURL = imgsURL;
    }

    public String[] getImgsURL() {
        return ImgsURL;
    }

    public int getNewsId() {
        return NewsId;
    }

    public void setNewsId(int id) {
        this.NewsId = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getViews() {
        return Views;
    }

    public void setViews(int views) {
        Views = views;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getSince() {
        return Since;
    }

    public void setSince(String since) {
        Since = since;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public int getNavIcon() {
        return NavIcon;
    }

    public String getNavTitle() {
        return NavTitle;
    }

    public String getOneImage() {
        return OneImage;
    }

    public void setOneImage(String oneImage) {
        OneImage = oneImage;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getSearchTitle() {
        return SearchTitle;
    }

    public void setSearchTitle(String searchTitle) {
        SearchTitle = searchTitle;
    }

    public int getSearchViews() {
        return SearchViews;
    }

    public void setSearchViews(int searchViews) {
        SearchViews = searchViews;
    }

    public String getSearchImage() {
        return SearchImage;
    }

    public void setSearchImage(String searchImage) {
        SearchImage = searchImage;
    }

    public String getSearchSince() {
        return SearchSince;
    }

    public void setSearchSince(String searchSince) {
        SearchSince = searchSince;
    }

    public int getSearchType() {
        return SearchType;
    }

    public void setSearchType(int searchType) {
        SearchType = searchType;
    }

    public int getSearchNewsId() {
        return SearchNewsId;
    }

    public void setSearchNewsId(int searchNewsId) {
        SearchNewsId = searchNewsId;
    }

    public String getOneImageShow() {
        return OneImageShow;
    }

    public void setOneImageShow(String oneImageShow) {
        OneImageShow = oneImageShow;
    }

    public String[] getImages() {
        return Images;
    }

    public void setImages(String[] images) {
        Images = images;
    }
}
