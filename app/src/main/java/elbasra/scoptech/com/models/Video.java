package elbasra.scoptech.com.models;

import java.io.Serializable;

/**
 * Created by ahmed on 26/12/16.
 */

public class Video implements Serializable{
    String title,url,youtube_id;
    int views,id;
    String SearchTitle,SearchUrl,SearchYoutube_id,date;
    int SearchViews;



    public void setYoutube_id(String youtube_id) {
        this.youtube_id = youtube_id;
    }

    public Video(int id ,String title, String url, int views, String youtube_id,String date) {
        this.title = title;
        this.url = url;
        this.views = views;
        this.youtube_id = youtube_id;
        this.id = id;
        this.date = date;

    }



//    public Video(int id, String searchTitle, String searchUrl, int searchViews , String searchYoutube_id , boolean check,String date) {
//        this.SearchTitle = searchTitle;
//        this.SearchUrl = searchUrl;
//        this.SearchYoutube_id = searchYoutube_id;
//        this.SearchViews = searchViews;
//        this.id = id;
//        this.date = date;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }
    public String getYoutube_id() {
        return youtube_id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

//    public String getSearchTitle() {
//        return SearchTitle;
//    }

//    public String getSearchUrl() {
//        return SearchUrl;
//    }

//    public String getSearchYoutube_id() {
//        return SearchYoutube_id;
//    }

//    public int getSearchViews() {
//        return SearchViews;
//    }
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
