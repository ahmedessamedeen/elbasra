package elbasra.scoptech.com.acitivities;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.adapters.NavListAdapter;
import elbasra.scoptech.com.adapters.SearchVideosAdapter;
import elbasra.scoptech.com.adapters.VideosAdapter;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.helpers.CheckInternet;
import elbasra.scoptech.com.helpers.HidingScrollListener;
import elbasra.scoptech.com.helpers.SharedPreference;
import elbasra.scoptech.com.models.Video;
import elbasra.scoptech.com.models.getData;

public class YoutubeVideos extends AppCompatActivity {
    RecyclerView recycle_videos;
    ArrayList<Video> dataList = new ArrayList<>();
    ArrayList<Video> saved_dataList = new ArrayList<>();
    ArrayList<Video> dataList2 = new ArrayList<>();
    DrawerLayout drawerLayout;
    VideosAdapter adapter;
    SearchVideosAdapter adapterS;
    LinearLayout header;
    FloatingActionButton fab_videos;
    FrameLayout main_frame;
    LinearLayout no_internet;
    SwipeRefreshLayout swipeRefreshLayout;
    EditText et_search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_videos);
        recycle_videos = (RecyclerView) findViewById(R.id.rv_list_videos);
        header = (LinearLayout) findViewById(R.id.header);
        fab_videos = (FloatingActionButton) findViewById(R.id.fab_add_vedio);
        et_search = (EditText) findViewById(R.id.et_search);
        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!et_search.getText().toString().equals("")) {
                        getVideosVolley("http://basra.magdsoft.com/searchVideo?name="+et_search.getText().toString());
                        searchAdapter();
                        return true;
                    }
                    Toast.makeText(YoutubeVideos.this, getString(R.string.error_field_empty), Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });
        searchAdapter();
        AllVideosAdapter();
        main_frame = (FrameLayout) findViewById(R.id.main_container);
        no_internet = (LinearLayout) findViewById(R.id.nointernet_container);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i("log", "onRefresh called from SwipeRefreshLayout");
                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        dataList.clear();
                        recreate();
                    }
                }
        );

        recycle_videos.setOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }
            @Override
            public void onShow() {
                showViews();
            }
        });
        if(CheckInternet.internetConnectionAvailable(3000)) {
            no_internet.setVisibility(View.GONE);
            NavAdapter();
        }
        else{
            no_internet.setVisibility(View.VISIBLE);
            main_frame.setVisibility(View.VISIBLE);

        }
        if(!CheckInternet.internetConnectionAvailable(3000)){
//            getSaveVideos();
        }
    }



    private String getYouTubeId (String youTubeUrl) {
        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if(matcher.find()){
            return matcher.group();
        } else {
            return "error";
        }
    }

    private void NavAdapter(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final LinearLayout drawerPanel = (LinearLayout) findViewById(R.id.drawer_panel);
        final ListView lvNav = (ListView) findViewById(R.id.nav_list);
        final List<getData> navList = new ArrayList<>();

        navList.add(new getData(getString(R.string.news), R.drawable.sm1));
        navList.add(new getData(getString(R.string.ads), R.drawable.sm2));
        navList.add(new getData(getString(R.string.videos), R.drawable.sm3));
        navList.add(new getData(getString(R.string.sport), R.drawable.sm4));
        navList.add(new getData(getString(R.string.various), R.drawable.sm5));
        navList.add(new getData(getString(R.string.send_news), R.drawable.sm6));
        navList.add(new getData(getString(R.string.contact_us), R.drawable.sm7));
        navList.add(new getData(getString(R.string.share) , R.drawable.bsra_share_app));

        NavListAdapter navListAdapter = new NavListAdapter(this , R.layout.custom_nav_list_item , navList);
        lvNav.setAdapter(navListAdapter);
        navListAdapter.notifyDataSetChanged();

        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goNews = new Intent(YoutubeVideos.this,News.class);
                switch (position){
                    case 0:
                        goNews.putExtra("type" , 1);
                        startActivity(goNews);
                        break;
                    case 1:
                        goNews.putExtra("type" , 2);
                        startActivity(goNews);
                        break;
                    case 2:
                        startActivity(new Intent(YoutubeVideos.this,YoutubeVideos.class).putExtra("search_text",""));
                        break;
                    case 3:
                        goNews.putExtra("type" , 3);
                        startActivity(goNews);
                        break;
                    case 4:
                        goNews.putExtra("type" , 4);
                        startActivity(goNews);
                        break;
                    case 5:
                        startActivity(new Intent(YoutubeVideos.this,SendNews.class));
                        break;
                    case 6:
                        startActivity(new Intent(YoutubeVideos.this,ContactUs.class));
                        break;
                    case 7:
                        Intent share = new Intent();
                        share.setAction(Intent.ACTION_SEND);
                        share.putExtra(Intent.EXTRA_TEXT , "https://play.google.com/store/apps/details?id=elbasra.scoptech.com");
                        share.setType("text/plain");
                        startActivity(share);
                        break;
                }
                setTitle(navList.get(position).getNavTitle());
                lvNav.setItemChecked(0, true);
                drawerLayout.closeDrawer(drawerPanel);
            }
        });
    }

    public void menu(View v){
        if(CheckInternet.internetConnectionAvailable(3000)) {
            no_internet.setVisibility(View.GONE);
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else{
            no_internet.setVisibility(View.VISIBLE);
            main_frame.setVisibility(View.VISIBLE);

        }
    }

    public void back(View v){
        finish();
    }

    public void addvideo(View view) {
        startActivity(new Intent(YoutubeVideos.this,AddVideo.class));
    }
    //http://basra.magdsoft.com/getVideo
    private void hideViews() {
        header.animate().translationY(-header.getHeight()).setInterpolator(new AccelerateInterpolator(2));

        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fab_videos.getLayoutParams();
        int fabBottomMargin = lp.bottomMargin;
        fab_videos.animate().translationY(fab_videos.getHeight()+fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
    }

    private void showViews() {
        header.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
        fab_videos.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }

    public void getVideosVolley(String url){

        final HashSet<String> recieved_videos_names=new HashSet<>();
        final HashSet<String> recieved_videos_urls=new HashSet<>();
        final HashSet<String> recieved_videos_views=new HashSet<>();
        final HashSet<String> recieved_videos_ids=new HashSet<>();

        swipeRefreshLayout.setRefreshing(true);
        dataList.clear();
        dataList2.clear();

        final String TAG = "your Data Return is :";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG , response.toString());
                try {
                    String status = response.getString("Status");
                    JSONArray dataArray = response.getJSONArray("Data");
                    if(status.equals("Done")) {
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            Video video = new Video(dataObject.getInt("id"),dataObject.getString("name"),dataObject.getString("link"),dataObject.getInt("views"),getYouTubeId(dataObject.getString("link")),dataObject.getString("date"));
                            dataList2.add(video);
                        }
                        adapterS.notifyDataSetChanged();
                        swipeRefreshLayout.setRefreshing(false);

                    }else if(status.equals("Nope")){
                        Toast.makeText(YoutubeVideos.this, "Error", Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(YoutubeVideos.this, ""+getString(R.string.error_loading), Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void AllVideosAdapter(){
        if (CheckInternet.internetConnectionAvailable(3000)){
            recycle_videos.setHasFixedSize(true);
            recycle_videos.setLayoutManager(new GridLayoutManager(this, 1));
            adapter = new VideosAdapter(this , dataList);
            recycle_videos.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }else{
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    public void searchAdapter(){
        dataList.clear();
        recycle_videos.setHasFixedSize(true);
        recycle_videos.setLayoutManager(new GridLayoutManager(this, 1));
        adapterS = new SearchVideosAdapter(this , dataList2);
        recycle_videos.setAdapter(adapterS);
        adapterS.notifyDataSetChanged();
    }

    public void search(View v) {
        if(!et_search.getText().toString().equals("")) {
            getVideosVolley("http://basra.magdsoft.com/searchVideo?name="+et_search.getText().toString());
            searchAdapter();
        }else{
            Toast.makeText(this, getString(R.string.error_field_empty), Toast.LENGTH_SHORT).show();
        }
    }

    public void refresh(View view) {
        recreate();
    }

    private void getSaveVideos() {
        Set<String> names,urls,views,youtube_ids,ids,dates;


        names = SharedPreference.getInstance().getValueSet(getApplicationContext(),"names");
        urls = SharedPreference.getInstance().getValueSet(getApplicationContext(),"urls");
        views = SharedPreference.getInstance().getValueSet(getApplicationContext(),"views");
        youtube_ids = SharedPreference.getInstance().getValueSet(getApplicationContext(),"ids");
        ids = SharedPreference.getInstance().getValueSet(getApplicationContext(),"ids");
        dates = SharedPreference.getInstance().getValueSet(getApplicationContext(),"dates");

        String[] videos_names = names.toArray(new String[names.size()]);
        String[] videos_urls = urls.toArray(new String[urls.size()]);
        String[] videos_views = views.toArray(new String[views.size()]);
        String[] videos_youtube_ids = youtube_ids.toArray(new String[youtube_ids.size()]);
        String[] videos_ids = ids.toArray(new String[ids.size()]);
        String[] videos_date = dates.toArray(new String[youtube_ids.size()]);

        for ( int i = 0;i<videos_names.length;i++){
            Video video = new Video(Integer.parseInt(videos_ids[i]),videos_names[i],videos_urls[i],Integer.parseInt(videos_views[i]),videos_youtube_ids[i],videos_date[i]);
            saved_dataList.add(video);
        }
        recycle_videos.setHasFixedSize(true);
        recycle_videos.setLayoutManager(new GridLayoutManager(this, 1));
        adapter = new VideosAdapter(this , saved_dataList);
        recycle_videos.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    private void saveSet(HashSet<String > videos,String key) {
        SharedPreference.getInstance().save(YoutubeVideos.this,videos,key);
    }

}
