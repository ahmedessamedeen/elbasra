package elbasra.scoptech.com.acitivities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.adapters.NavListAdapter;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.helpers.CheckInternet;
import elbasra.scoptech.com.helpers.SharedPreference;
import elbasra.scoptech.com.models.getData;


public class ContactUs extends AppCompatActivity {
    Context mcontext;
    AutoCompleteTextView user_info,title,content;
    DrawerLayout drawerLayout;
    TextView mails,phoneNums;
    Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        content = (AutoCompleteTextView) findViewById(R.id.content);
        title = (AutoCompleteTextView) findViewById(R.id.title);
        user_info = (AutoCompleteTextView) findViewById(R.id.user_info);
        send = (Button) findViewById(R.id.btn_ContactUs);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attempSend();
            }
        });
        NavAdapter();
        getContacts("http://basra.magdsoft.com/getContact");
    }
    private void NavAdapter(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final LinearLayout drawerPanel = (LinearLayout) findViewById(R.id.drawer_panel);
        final ListView lvNav = (ListView) findViewById(R.id.nav_list);
        final List<getData> navList = new ArrayList<>();

        navList.add(new getData(getString(R.string.news), R.drawable.sm1));
        navList.add(new getData(getString(R.string.ads), R.drawable.sm2));
        navList.add(new getData(getString(R.string.videos), R.drawable.sm3));
        navList.add(new getData(getString(R.string.sport), R.drawable.sm4));
        navList.add(new getData(getString(R.string.various), R.drawable.sm5));
        navList.add(new getData(getString(R.string.send_news), R.drawable.sm6));
        navList.add(new getData(getString(R.string.contact_us), R.drawable.sm7));
        navList.add(new getData(getString(R.string.share) , R.drawable.bsra_share_app));

        NavListAdapter navListAdapter = new NavListAdapter(this , R.layout.custom_nav_list_item , navList);
        lvNav.setAdapter(navListAdapter);
        navListAdapter.notifyDataSetChanged();
        mails = (TextView) findViewById(R.id.tv_mails);
        phoneNums = (TextView) findViewById(R.id.tv_phones);
        if(!CheckInternet.internetConnectionAvailable(3000)) Initiate_Contacts();
        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goNews = new Intent(ContactUs.this,News.class);
                switch (position){
                    case 0:
                        goNews.putExtra("type" , 1);
                        startActivity(goNews);
                        break;
                    case 1:
                        goNews.putExtra("type" , 2);
                        startActivity(goNews);
                        break;
                    case 2:
                        startActivity(new Intent(ContactUs.this,YoutubeVideos.class).putExtra("search_text",""));
                        break;
                    case 3:
                        goNews.putExtra("type" , 3);
                        startActivity(goNews);
                        break;
                    case 4:
                        goNews.putExtra("type" , 4);
                        startActivity(goNews);
                        break;
                    case 5:
                        startActivity(new Intent(ContactUs.this,SendNews.class));
                        break;
                    case 6:
                        startActivity(new Intent(ContactUs.this,ContactUs.class));
                        break;
                    case 7:
                        Intent share = new Intent();
                        share.setAction(Intent.ACTION_SEND);
                        share.putExtra(Intent.EXTRA_TEXT , "https://play.google.com/store/apps/details?id=elbasra.scoptech.com");
                        share.setType("text/plain");
                        startActivity(share);
                        break;
                }
                setTitle(navList.get(position).getNavTitle());
                lvNav.setItemChecked(0, true);
                drawerLayout.closeDrawer(drawerPanel);
            }
        });
    }



    public void menu(View v){
        drawerLayout.openDrawer(Gravity.RIGHT);
    }
    public void back(View v){
        finish();
    }

    public void contactUs(String url){
        final String TAG = "your Data Return is :";
        final HashMap<String, String> params = new HashMap<>();
        params.put("phone", user_info.getText().toString());
        params.put("address", title.getText().toString());
        params.put("description",content.getText().toString());
        params.put("email",user_info.getText().toString());
        Log.e("params is  :", params.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url,new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG , response.toString());
                try {
                    String status = response.getString("Status");
                    if(status.equals("Done")) {
                        Toast.makeText(ContactUs.this, ""+getString(R.string.sendContact), Toast.LENGTH_SHORT).show();
                        user_info.setText("");
                        title.setText("");
                        content.setText("");
                    }else{
                        Toast.makeText(ContactUs.this, "Error", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(AddAdvert.this, error.getMessage(), Toast.LENGTH_LONG).show();
                if (error instanceof NoConnectionError) {
                    Log.d("NoConnectionError>>>>", "NoConnectionError.......");
                } else if (error instanceof AuthFailureError) {
                    Log.d("AuthFailureError>>>>", "AuthFailureError.......");
                } else if (error instanceof ServerError) {
                    Log.d("ServerError>>>>>>>>>", "ServerError.......");
                } else if (error instanceof NetworkError) {
                    Log.d("NetworkError>>>>>>>>>", "NetworkError.......");
                } else if (error instanceof ParseError) {
                    Log.d("ParseError>>>>>>>>>", "ParseError.......");
                } else if (error instanceof TimeoutError) {
                    Log.d("TimeoutError>>>>>>>>>", "TimeoutError.......");
                }
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
    public void getContacts(String url){
        final String TAG = "your Data Return is :";
        final HashMap<String, String> params = new HashMap<>();
        Log.e("params is  :", params.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url,new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG , response.toString());
                try {
                    String status = response.getString("Status");
                    if(status.equals("Done")) {
                        JSONObject data = response.getJSONObject("Data");
                        JSONArray emails = data.getJSONArray("email");
                        for (int i =0;i<emails.length();i++){
                            mails.setText(mails.getText()+"\n"+emails.getString(i));
                        }
                        JSONArray phones = data.getJSONArray("phone");
                        for (int i =0;i<emails.length();i++){
                            phoneNums.setText(phoneNums.getText()+"\n"+phones.getString(i));
                        }
                        SharedPreference.getInstance().save(ContactUs.this,mails.getText().toString(),getString(R.string.basra_mails));
                        SharedPreference.getInstance().save(ContactUs.this,phoneNums.getText().toString(),getString(R.string.basra_phones));



                    }else{
                        Toast.makeText(ContactUs.this, "Error", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(AddAdvert.this, error.getMessage(), Toast.LENGTH_LONG).show();
                if (error instanceof NoConnectionError) {
                    Log.d("NoConnectionError>>>>", "NoConnectionError.......");
                } else if (error instanceof AuthFailureError) {
                    Log.d("AuthFailureError>>>>", "AuthFailureError.......");
                } else if (error instanceof ServerError) {
                    Log.d("ServerError>>>>>>>>>", "ServerError.......");
                } else if (error instanceof NetworkError) {
                    Log.d("NetworkError>>>>>>>>>", "NetworkError.......");
                } else if (error instanceof ParseError) {
                    Log.d("ParseError>>>>>>>>>", "ParseError.......");
                } else if (error instanceof TimeoutError) {
                    Log.d("TimeoutError>>>>>>>>>", "TimeoutError.......");
                }
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    public void attempSend(){
        boolean cancel = false;
        View focusView = null;
        content.setError(null);
        user_info.setError(null);
        title.setError(null);
        if(CheckInternet.internetConnectionAvailable(3000)){
            if (TextUtils.isEmpty(user_info.getText().toString())) {
                user_info.setError(getString(R.string.error_field_required));
                focusView = user_info;
                cancel = true;
            }
             if (TextUtils.isEmpty(title.getText().toString())) {
                title.setError(getString(R.string.error_field_required));
                focusView = title;
                cancel = true;
            }
             if (TextUtils.isEmpty(content.getText().toString())) {
                content.setError(getString(R.string.error_field_required));
                focusView = content;
                cancel = true;
            }
            else{
                contactUs("http://basra.magdsoft.com/sendContact");
            }

        }
        else{
            Toast.makeText(ContactUs.this, ""+"لا يوجد إتصال بالإنترنت", Toast.LENGTH_SHORT).show();
        }
    }
    private void Initiate_Contacts() {
        mails.setText(SharedPreference.getInstance().getValue(getApplicationContext(), getString(R.string.basra_mails)));
        phoneNums.setText(SharedPreference.getInstance().getValue(getApplicationContext(), getString(R.string.basra_phones)));
    }

}
