package elbasra.scoptech.com.acitivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import elbasra.scoptech.com.R;

public class Welcome extends AppCompatActivity implements View.OnClickListener {
    ImageView iv_news,iv_ads,iv_videos,iv_sport,iv_misc,iv_sendNews,iv_contact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        iv_news = (ImageView) findViewById(R.id.iv_s1);
        iv_ads = (ImageView) findViewById(R.id.iv_s2);
        iv_videos = (ImageView) findViewById(R.id.iv_s3);
        iv_sport = (ImageView) findViewById(R.id.iv_s4);
        iv_misc = (ImageView) findViewById(R.id.iv_s5);
        iv_sendNews = (ImageView) findViewById(R.id.iv_s6);
        iv_contact = (ImageView) findViewById(R.id.iv_s7);
        iv_news.setOnClickListener(this);
        iv_ads.setOnClickListener(this);
        iv_videos.setOnClickListener(this);
        iv_sport.setOnClickListener(this);
        iv_misc.setOnClickListener(this);
        iv_sendNews.setOnClickListener(this);
        iv_contact.setOnClickListener(this);
//        printKeyHash();
    }


    @Override
    public void onClick(View v) {
        Intent goNews = new Intent(Welcome.this,News.class);
        if(v == iv_news){
            goNews.putExtra("type" , 1);
            startActivity(goNews);
        } else if(v == iv_ads){
            goNews.putExtra("type" , 2);
            startActivity(goNews);
        }else if(v == iv_videos){
            startActivity(new Intent(Welcome.this,YoutubeVideos.class).putExtra("search_text",""));
        } else if(v == iv_sport){
            goNews.putExtra("type" , 3);
            startActivity(goNews);
        } else if(v == iv_misc){
            goNews.putExtra("type" , 4);
            startActivity(goNews);
        } else if(v == iv_sendNews){
            startActivity(new Intent(Welcome.this,SendNews.class));
        } else if(v == iv_contact){
            startActivity(new Intent(Welcome.this,ContactUs.class));
        }

    }

    public void printKeyHash(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "elbasra.scoptech.com",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Hash : " , e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("Hash : " , e.toString());
        }
    }
}
