package elbasra.scoptech.com.acitivities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.adapters.CommentAdapter;
import elbasra.scoptech.com.adapters.NavListAdapter;
import elbasra.scoptech.com.adapters.OneNewsAdapterImage;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.models.getData;

public class OneNews extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    public static final String URL_SEND_COMMENT = "http://basra.magdsoft.com/sendComment";
    public static final String URL_COMMENTS = "http://basra.magdsoft.com/getCommentsOnContent?id=";
    public static final String URL_VIEWS = "http://basra.magdsoft.com/addViews";
    public static int User_ID;
    public static int TypeID;
    Uri imageUri;
    RecyclerView rv_recycler;
    CommentAdapter adapter;
    GridView gridview_display_subCats;
    public static ArrayList<getData> myDataset = new ArrayList<>();
    TextView tv_header_title, tv_title, tv_content, tv_views, tv_since, tv_link;
    ImageView iv_title_image;
    ImageButton bt_comment, ib_share;
    EditText et_comment;
    DrawerLayout drawerLayout;
    LinearLayout linear_progress;
    ProgressBar progressImage;
    String ImageURL;
    String Content;
    public String extractedurl, longText;
    SpannableString ss;
    getData oneNews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_news);

        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_content = (TextView) findViewById(R.id.tv_content);
        tv_views = (TextView) findViewById(R.id.tv_views);
        tv_since = (TextView) findViewById(R.id.tv_since);
        tv_link = (TextView) findViewById(R.id.tv_link);
        iv_title_image = (ImageView) findViewById(R.id.iv_title_image);
        et_comment = (EditText) findViewById(R.id.et_comment);
        bt_comment = (ImageButton) findViewById(R.id.bt_comment);
        ib_share = (ImageButton) findViewById(R.id.ib_share);
        rv_recycler = (RecyclerView) findViewById(R.id.rv_comment);
        linear_progress = (LinearLayout) findViewById(R.id.progressBar);
        progressImage = (ProgressBar) findViewById(R.id.progressImage);
        rv_recycler.setNestedScrollingEnabled(false);
        NavAdapter();
        User_ID = getIntent().getIntExtra("newsid", 0);
        TypeID = getIntent().getIntExtra("type", 0);
        oneNews = (getData) getIntent().getSerializableExtra("news");
        RecyclerCommentAdapter();
        bt_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_comment.getText().toString().equals("")) {
                    AddComment(et_comment.getText().toString());
                    et_comment.setText("");
                } else {
                    Toast.makeText(OneNews.this, getString(R.string.error_comment_empty), Toast.LENGTH_SHORT).show();
                }
            }
        });

        ib_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();
                new shareBitmapTask().execute(ImageURL);
            }
        });

        if (TypeID == 1) {
            tv_header_title.setText(getString(R.string.news));
        } else if (TypeID == 2) {
            tv_header_title.setText(getString(R.string.ads));
        } else if (TypeID == 3) {
            tv_header_title.setText(getString(R.string.sport));
        } else if (TypeID == 4) {
            tv_header_title.setText(getString(R.string.various));
        }
        iv_title_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OneNews.this, OneImageShow.class).putExtra("image", ImageURL));
            }
        });
        setNewsData();
    }

    public void setNewsData() {
        Glide.with(OneNews.this)
                .load(oneNews.getImage())
                .error(R.drawable.image_holder)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressImage.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressImage.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(iv_title_image);

        Content = oneNews.getContent();
        ImageURL = oneNews.getImage();
        extractedurl = extractUrls(Content);
        longText = extractedurl;
        int index = searchForText(Content, longText);
        ss = new SpannableString(Content);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(extractedurl));
                startActivity(i);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        if (index > 0) {
            ss.setSpan(clickableSpan, index, index + longText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        tv_content.setText(ss);
        tv_content.setMovementMethod(LinkMovementMethod.getInstance());
        tv_content.setHighlightColor(Color.TRANSPARENT);
        tv_title.setText(oneNews.getTitle());
        tv_since.setText(oneNews.getSince());
        tv_views.setText(Integer.toString(oneNews.getViews()));
        GridView(oneNews.getImages());
        addViews();
    }

    public void AddComment(final String text) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("content_id", Integer.toString(User_ID));
        params.put("content", text);
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(URL_SEND_COMMENT
                    , new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("your Comment Is :", response.toString());
                    try {
                        String status = response.getString("Status");
                        if (status.equals("Done")) {
                            Toast toast = Toast.makeText(OneNews.this, getString(R.string.send_comment), Toast.LENGTH_SHORT);
                            ((TextView) ((LinearLayout) toast.getView()).getChildAt(0))
                                    .setGravity(Gravity.CENTER_HORIZONTAL);
                            toast.show();
                        } else {
                            Toast.makeText(OneNews.this, getString(R.string.error_comment), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Log.e("exception 1", e.toString());
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(OneNews.this, getString(R.string.error_comment), Toast.LENGTH_SHORT).show();
                    Log.e("exception 1", error.toString());

                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.e("exception 1", e.toString());
            e.printStackTrace();
        }
    }

    public void GridView(String[] imageArray) {
        //grid view of image
        myDataset.clear();
        gridview_display_subCats = (GridView) findViewById(R.id.gridview_display_SubCat);


        for (int i = 0; i < imageArray.length; i++) {
            getData getData = new getData();
            getData.setOneImage(imageArray[i]);
            myDataset.add(getData);
        }
        gridview_display_subCats.setAdapter(new OneNewsAdapterImage(OneNews.this, myDataset));
    }

    public void RecyclerCommentAdapter() {
        rv_recycler.setHasFixedSize(true);
        rv_recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CommentAdapter(OneNews.this, URL_COMMENTS + User_ID + "&type=" + Integer.toString(TypeID));
        rv_recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void addViews() {
        final String TAG = "your add View";
        HashMap<String, String> params = new HashMap<>();
        params.put("id", Integer.toString(User_ID));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(URL_VIEWS, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
                if (tv_views.getText().toString().equals("") | Content.equals("") | ImageURL.equals("")
                        | tv_title.getText().toString().equals("") | tv_since.getText().toString().equals("")) {
                } else {
                    linear_progress.setVisibility(View.GONE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void NavAdapter() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final LinearLayout drawerPanel = (LinearLayout) findViewById(R.id.drawer_panel);
        final ListView lvNav = (ListView) findViewById(R.id.nav_list);
        final List<getData> navList = new ArrayList<>();

        navList.add(new getData(getString(R.string.news), R.drawable.sm1));
        navList.add(new getData(getString(R.string.ads), R.drawable.sm2));
        navList.add(new getData(getString(R.string.videos), R.drawable.sm3));
        navList.add(new getData(getString(R.string.sport), R.drawable.sm4));
        navList.add(new getData(getString(R.string.various), R.drawable.sm5));
        navList.add(new getData(getString(R.string.send_news), R.drawable.sm6));
        navList.add(new getData(getString(R.string.contact_us), R.drawable.sm7));
        navList.add(new getData(getString(R.string.share) , R.drawable.bsra_share_app));

        NavListAdapter navListAdapter = new NavListAdapter(this, R.layout.custom_nav_list_item, navList);
        lvNav.setAdapter(navListAdapter);
        navListAdapter.notifyDataSetChanged();

        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goNews = new Intent(OneNews.this, News.class);
                switch (position) {
                    case 0:
                        goNews.putExtra("type", 1);
                        startActivity(goNews);
                        break;
                    case 1:
                        goNews.putExtra("type", 2);
                        startActivity(goNews);
                        break;
                    case 2:
                        startActivity(new Intent(OneNews.this, YoutubeVideos.class).putExtra("search_text", ""));
                        break;
                    case 3:
                        goNews.putExtra("type", 3);
                        startActivity(goNews);
                        break;
                    case 4:
                        goNews.putExtra("type", 4);
                        startActivity(goNews);
                        break;
                    case 5:
                        startActivity(new Intent(OneNews.this, SendNews.class));
                        break;
                    case 6:
                        startActivity(new Intent(OneNews.this, ContactUs.class));
                        break;
                    case 7:
                        Intent share = new Intent();
                        share.setAction(Intent.ACTION_SEND);
                        share.putExtra(Intent.EXTRA_TEXT , "https://play.google.com/store/apps/details?id=elbasra.scoptech.com");
                        share.setType("text/plain");
                        startActivity(share);
                        break;
                }
                setTitle(navList.get(position).getNavTitle());
                lvNav.setItemChecked(0, true);
                drawerLayout.closeDrawer(drawerPanel);
            }
        });
    }

    public void back(View v) {
        finish();
    }

    public void menu(View v) {
        drawerLayout.openDrawer(Gravity.RIGHT);
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
    }

    class shareBitmapTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {

            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            String path = "";
            final Intent share = new Intent();
            try {
                share.setAction(Intent.ACTION_SEND);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                path = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Title", null);
                imageUri = Uri.parse(path);
                share.putExtra(android.content.Intent.EXTRA_SUBJECT, oneNews.getTitle());
                share.putExtra(Intent.EXTRA_TEXT, oneNews.getTitle());
                share.putExtra(Intent.EXTRA_STREAM, imageUri);
                share.setType("*/*");
                startActivity(Intent.createChooser(share, "Share Image"));
            } catch (Exception e) {
                Log.e("error share is :", e.toString());
                Toast.makeText(OneNews.this, getString(R.string.excption_null), Toast.LENGTH_SHORT).show();
            }

        }
    }

    /*public static List<String> extractUrls(String text)
    {
        List<String> containedUrls = new ArrayList<String>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);

        while (urlMatcher.find())
        {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }

        return containedUrls;
    }*/
    public static String extractUrls(String text) {
        String containedUrls = "";
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);
        while (urlMatcher.find()) {
            containedUrls = text.substring(urlMatcher.start(0), urlMatcher.end(0));
        }

        return containedUrls;
    }

    public int searchForText(String text, String searc_word) {
        String strOrig = text;
        int intIndex = strOrig.indexOf(searc_word);
        if (intIndex == -1) {
            System.out.println("Hello not found");
        } else {
            System.out.println("Found Hello at index "
                    + intIndex);
        }
        return intIndex;

    }
    // Can be triggered by a view event such as a button press


}