package elbasra.scoptech.com.acitivities;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.adapters.NavListAdapter;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.helpers.CheckInternet;
import elbasra.scoptech.com.models.getData;

public class AddVideo extends AppCompatActivity {
    DrawerLayout drawerLayout;
    EditText title,link;
    Button send;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_video);
        NavAdapter();
        title =(EditText) findViewById(R.id.et_video_title);
        link =(EditText) findViewById(R.id.et_video_url);
        send = (Button) findViewById(R.id.uploadVideo);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CheckInternet.internetConnectionAvailable(3000)){
                     getVideosVolley("http://basra.magdsoft.com/sendVideo");}
                else{
                    Toast.makeText(AddVideo.this, ""+getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
    private void NavAdapter(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final LinearLayout drawerPanel = (LinearLayout) findViewById(R.id.drawer_panel);
        final ListView lvNav = (ListView) findViewById(R.id.nav_list);
        final List<getData> navList = new ArrayList<>();

        navList.add(new getData(getString(R.string.news), R.drawable.sm1));
        navList.add(new getData(getString(R.string.ads), R.drawable.sm2));
        navList.add(new getData(getString(R.string.videos), R.drawable.sm3));
        navList.add(new getData(getString(R.string.sport), R.drawable.sm4));
        navList.add(new getData(getString(R.string.various), R.drawable.sm5));
        navList.add(new getData(getString(R.string.send_news), R.drawable.sm6));
        navList.add(new getData(getString(R.string.contact_us), R.drawable.sm7));
        navList.add(new getData(getString(R.string.share) , R.drawable.bsra_share_app));

        NavListAdapter navListAdapter = new NavListAdapter(this , R.layout.custom_nav_list_item , navList);
        lvNav.setAdapter(navListAdapter);
        navListAdapter.notifyDataSetChanged();

        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goNews = new Intent(AddVideo.this,News.class);
                switch (position){
                    case 0:
                        goNews.putExtra("type" , 1);
                        startActivity(goNews);
                        break;
                    case 1:
                        goNews.putExtra("type" , 2);
                        startActivity(goNews);
                        break;
                    case 2:
                        startActivity(new Intent(AddVideo.this,YoutubeVideos.class).putExtra("search_text",""));
                        break;
                    case 3:
                        goNews.putExtra("type" , 3);
                        startActivity(goNews);
                        break;
                    case 4:
                        goNews.putExtra("type" , 4);
                        startActivity(goNews);
                        break;
                    case 5:
                        startActivity(new Intent(AddVideo.this,SendNews.class));
                        break;
                    case 6:
                        startActivity(new Intent(AddVideo.this,ContactUs.class));
                        break;
                    case 7:
                        Intent share = new Intent();
                        share.setAction(Intent.ACTION_SEND);
                        share.putExtra(Intent.EXTRA_TEXT , "https://play.google.com/store/apps/details?id=elbasra.scoptech.com");
                        share.setType("text/plain");
                        startActivity(share);
                        break;
                }
                setTitle(navList.get(position).getNavTitle());
                lvNav.setItemChecked(0, true);
                drawerLayout.closeDrawer(drawerPanel);
            }
        });
    }
    public void menu(View v){
        drawerLayout.openDrawer(Gravity.RIGHT);
    }
    public void back(View v){
        finish();
    }
    public void getVideosVolley(String url){
        final String TAG = "your Data Return is :";
        final HashMap<String, String> params = new HashMap<>();
        params.put("name", title.getText().toString());
        params.put("link", link.getText().toString());
        Log.e("params is  :", params.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url,new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG , response.toString());
                try {
                    String status = response.getString("Status");
                    if(status.equals("Done")) {
                        Toast.makeText(AddVideo.this, ""+getString(R.string.uploadedsuccessfully), Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(AddVideo.this, "Error", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(AddAdvert.this, error.getMessage(), Toast.LENGTH_LONG).show();
                if (error instanceof NoConnectionError) {
                    Log.d("NoConnectionError>>>>", "NoConnectionError.......");
                } else if (error instanceof AuthFailureError) {
                    Log.d("AuthFailureError>>>>", "AuthFailureError.......");
                } else if (error instanceof ServerError) {
                    Log.d("ServerError>>>>>>>>>", "ServerError.......");
                } else if (error instanceof NetworkError) {
                    Log.d("NetworkError>>>>>>>>>", "NetworkError.......");
                } else if (error instanceof ParseError) {
                    Log.d("ParseError>>>>>>>>>", "ParseError.......");
                } else if (error instanceof TimeoutError) {
                    Log.d("TimeoutError>>>>>>>>>", "TimeoutError.......");
                }
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

}
