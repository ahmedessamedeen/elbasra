package elbasra.scoptech.com.acitivities;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.adapters.NavListAdapter;
import elbasra.scoptech.com.adapters.RecAdapter;
import elbasra.scoptech.com.adapters.SearchAdapter;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.helpers.CheckInternet;
import elbasra.scoptech.com.models.getData;
import elbasra.scoptech.com.sync.Config;

public class News extends AppCompatActivity {

    public static final String ALL_NEWS_URL = "http://basra.magdsoft.com/getAllNews?page=";
    public static final String ALL_NEWS_URL2 = "http://basra.magdsoft.com/getContent?type=";
    public static final String URL_SEARCH_NAME = "http://basra.magdsoft.com/searchContent";
    //http://basra.magdsoft.com/searchContent?content=%D8%B5%D9%88%D8%AF%D8%B1%D8%AA
    public static int TypeID;
    RecyclerView rv_news;
    ArrayList<getData> listData = new ArrayList<>();
    ArrayList<getData> listData2 = new ArrayList<>();
    RecAdapter adapter;
    TextView tv_header_title;
    SearchAdapter searchAdapter;
    DrawerLayout drawerLayout;
    SwipeRefreshLayout refresh;
    EditText et_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        et_search = (EditText) findViewById(R.id.et_search);
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        refresh = (SwipeRefreshLayout) findViewById(R.id.refresh);
        rv_news = (RecyclerView) findViewById(R.id.rv_list);
        AllNewsAdapter();
        NavAdapter();

        TypeID = getIntent().getIntExtra("type", 0);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recreate();
            }
        });

        if (TypeID == 1) {
            tv_header_title.setText(getString(R.string.news));
        } else if (TypeID == 2) {
            tv_header_title.setText(getString(R.string.ads));
        } else if (TypeID == 3) {
            tv_header_title.setText(getString(R.string.sport));
        } else if (TypeID == 4) {
            tv_header_title.setText(getString(R.string.various));
        }

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!et_search.getText().toString().equals("")) {
                        if (CheckInternet.internetConnectionAvailable(3000)) {

                            searchVolley(et_search.getText().toString());
                            SearchAdapterN();
                            return true;
                        } else {
                            Toast.makeText(News.this, "لا يوجد إتصال بالإنترنت", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(News.this, getString(R.string.error_field_empty), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public void AllNewsAdapter() {
        if (CheckInternet.internetConnectionAvailable(3000)) {
            rv_news.setHasFixedSize(true);
            rv_news.setLayoutManager(new LinearLayoutManager(this));
            adapter = new RecAdapter(this, listData);
            rv_news.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    public void SearchAdapterN() {
        listData.clear();
        rv_news.setHasFixedSize(true);
        rv_news.setLayoutManager(new LinearLayoutManager(this));
        searchAdapter = new SearchAdapter(this, listData2);
        rv_news.setAdapter(searchAdapter);
        searchAdapter.notifyDataSetChanged();
    }

    private void NavAdapter() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final LinearLayout drawerPanel = (LinearLayout) findViewById(R.id.drawer_panel);
        final ListView lvNav = (ListView) findViewById(R.id.nav_list);
        final List<getData> navList = new ArrayList<>();

        navList.add(new getData(getString(R.string.news), R.drawable.sm1));
        navList.add(new getData(getString(R.string.ads), R.drawable.sm2));
        navList.add(new getData(getString(R.string.videos), R.drawable.sm3));
        navList.add(new getData(getString(R.string.sport), R.drawable.sm4));
        navList.add(new getData(getString(R.string.various), R.drawable.sm5));
        navList.add(new getData(getString(R.string.send_news), R.drawable.sm6));
        navList.add(new getData(getString(R.string.contact_us), R.drawable.sm7));
        navList.add(new getData(getString(R.string.share), R.drawable.bsra_share_app));

        NavListAdapter navListAdapter = new NavListAdapter(this, R.layout.custom_nav_list_item, navList);
        lvNav.setAdapter(navListAdapter);
        navListAdapter.notifyDataSetChanged();

        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goNews = new Intent(News.this, News.class);
                switch (position) {
                    case 0:
                        goNews.putExtra("type", 1);
                        startActivity(goNews);
                        break;
                    case 1:
                        goNews.putExtra("type", 2);
                        startActivity(goNews);
                        break;
                    case 2:
                        startActivity(new Intent(News.this, YoutubeVideos.class).putExtra("search_text", ""));
                        break;
                    case 3:
                        goNews.putExtra("type", 3);
                        startActivity(goNews);
                        break;
                    case 4:
                        goNews.putExtra("type", 4);
                        startActivity(goNews);
                        break;
                    case 5:
                        startActivity(new Intent(News.this, SendNews.class));
                        break;
                    case 6:
                        startActivity(new Intent(News.this, ContactUs.class));
                        break;
                    case 7:
                        Intent share = new Intent();
                        share.setAction(Intent.ACTION_SEND);
                        share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=elbasra.scoptech.com");
                        share.setType("text/plain");
                        startActivity(share);
                        break;
                }
                setTitle(navList.get(position).getNavTitle());
                lvNav.setItemChecked(0, true);
                drawerLayout.closeDrawer(drawerPanel);
            }
        });
    }

    public void searchVolley(String text) {
        refresh.setRefreshing(true);
        listData.clear();
        listData2.clear();
        final String TAG = "your search Respone :";
        HashMap<String, String> params = new HashMap<>();
        params.put("text", text);
        params.put("type", Integer.toString(TypeID));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(URL_SEARCH_NAME, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
                try {
                    String Status = response.getString("Status");
                    if (Status.equals("Done")) {
                        JSONArray dataArray = response.getJSONArray("Data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            getData getData = new getData();
                            getData.setSearchNewsId(dataObject.getInt("id"));
                            getData.setSearchTitle(dataObject.getString("name"));
                            getData.setSearchImage(dataObject.getString("image"));
                            getData.setSearchSince(dataObject.getString("date"));
                            getData.setSearchViews(dataObject.getInt("views"));
                            getData.setSearchType(dataObject.getInt("type"));
                            listData2.add(getData);
                        }
                        searchAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(News.this, getString(R.string.error_search), Toast.LENGTH_SHORT).show();
                    }
                    refresh.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void search(View v) {
        if (!et_search.getText().toString().equals("")) {
            if (CheckInternet.internetConnectionAvailable(3000)) {
                searchVolley(et_search.getText().toString());
                SearchAdapterN();
            } else {
                Toast.makeText(this, "لا يوجد إتصال بالإنترنت", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.error_field_empty), Toast.LENGTH_SHORT).show();
        }
    }

    public void back(View v) {
        finish();
    }

    public void menu(View v) {
        drawerLayout.openDrawer(Gravity.RIGHT);
    }
}
