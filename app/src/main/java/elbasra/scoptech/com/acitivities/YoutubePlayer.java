package elbasra.scoptech.com.acitivities;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.adapters.CommentAdapter;
import elbasra.scoptech.com.adapters.NavListAdapter;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.models.getData;
import elbasra.scoptech.com.sync.Config;

public class YoutubePlayer extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    public static final String URL_VIDEO_VIEWS = "http://basra.magdsoft.com/addViewVideo";
    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youTubeView;
    TextView video_title;
    String video_url,video_id , id;
    DrawerLayout drawerLayout;
    RecyclerView rv_recycler;
    CommentAdapter adapter;
    ImageButton bt_comment, ib_share;
    EditText et_comment;
    public static int User_ID;
    public static final String URL_SEND_COMMENT = "http://basra.magdsoft.com/sendComment";
    public static final String URL_ONE_Video = "http://basra.magdsoft.com/getOneVideo?video_id=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_player);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(Config.YOUTUBE_API_KEY, this);
        video_title = (TextView) findViewById(R.id.tv_video_title);
        video_url = getIntent().getStringExtra("url");
        video_id = getIntent().getStringExtra("youtube_id");
        id = getIntent().getStringExtra("id");
        video_title.setText(getIntent().getStringExtra("title"));
        NavAdapter();
        rv_recycler = (RecyclerView) findViewById(R.id.rv_comment);
        rv_recycler.setNestedScrollingEnabled(false);
        RecyclerCommentAdapter();
        et_comment = (EditText) findViewById(R.id.et_comment);

        bt_comment = (ImageButton) findViewById(R.id.bt_comment);

        bt_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_comment.getText().toString().equals("")) {
                    AddComment(et_comment.getText().toString());
                } else {
                    Toast.makeText(YoutubePlayer.this, getString(R.string.error_comment_empty), Toast.LENGTH_SHORT).show();
                }
            }
        });
        addVideoViews();
    }

    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            player.cueVideo(video_id); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
        }
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = getResources().getString(R.string.player_error)+ errorReason.toString();
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(Config.YOUTUBE_API_KEY, this);
        }
    }

    protected Provider getYouTubePlayerProvider() {
        return youTubeView;
    }
    private void NavAdapter(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final LinearLayout drawerPanel = (LinearLayout) findViewById(R.id.drawer_panel);
        final ListView lvNav = (ListView) findViewById(R.id.nav_list);
        final List<getData> navList = new ArrayList<>();

        navList.add(new getData(getString(R.string.news), R.drawable.sm1));
        navList.add(new getData(getString(R.string.ads), R.drawable.sm2));
        navList.add(new getData(getString(R.string.videos), R.drawable.sm3));
        navList.add(new getData(getString(R.string.sport), R.drawable.sm4));
        navList.add(new getData(getString(R.string.various), R.drawable.sm5));
        navList.add(new getData(getString(R.string.send_news), R.drawable.sm6));
        navList.add(new getData(getString(R.string.contact_us), R.drawable.sm7));
        navList.add(new getData(getString(R.string.share) , R.drawable.bsra_share_app));

        NavListAdapter navListAdapter = new NavListAdapter(this , R.layout.custom_nav_list_item , navList);
        lvNav.setAdapter(navListAdapter);
        navListAdapter.notifyDataSetChanged();

        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goNews = new Intent(YoutubePlayer.this, News.class);
                switch (position){
                    case 0:
                        goNews.putExtra("type", 1);
                        startActivity(goNews);
                        break;
                    case 1:
                        goNews.putExtra("type", 2);
                        startActivity(goNews);
                        break;
                    case 2:
                        finish();
                        startActivity(new Intent(YoutubePlayer.this,YoutubeVideos.class));
                        break;
                    case 3:
                        goNews.putExtra("type", 3);
                        startActivity(goNews);
                        break;
                    case 4:
                        goNews.putExtra("type", 4);
                        startActivity(goNews);
                        break;
                    case 5:
                        startActivity(new Intent(YoutubePlayer.this,SendNews.class));
                        break;
                    case 6:
                        startActivity(new Intent(YoutubePlayer.this,ContactUs.class));
                        break;
                    case 7:
                        Intent share = new Intent();
                        share.setAction(Intent.ACTION_SEND);
                        share.putExtra(Intent.EXTRA_TEXT , "https://play.google.com/store/apps/details?id=elbasra.scoptech.com");
                        share.setType("text/plain");
                        startActivity(share);
                        break;
                }
                setTitle(navList.get(position).getNavTitle());
                lvNav.setItemChecked(0, true);
                drawerLayout.closeDrawer(drawerPanel);
            }
        });
    }

    public void back(View v){
        finish();
    }
    public void RecyclerCommentAdapter() {
        rv_recycler.setHasFixedSize(true);
        rv_recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CommentAdapter(YoutubePlayer.this,URL_ONE_Video+getIntent().getStringExtra("id"));
        rv_recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    public void AddComment(String text) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("video_id", getIntent().getStringExtra("id"));
        params.put("content", text);
        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(URL_SEND_COMMENT
                        , new JSONObject(params), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("your Comment Is :", response.toString());
                        try {
                            String status = response.getString("Status");
                            if (status.equals("Done")) {
                                Toast toast = Toast.makeText(YoutubePlayer.this, getString(R.string.send_comment),
                                        Toast.LENGTH_SHORT);
                                ((TextView) ((LinearLayout) toast.getView()).getChildAt(0))
                                        .setGravity(Gravity.CENTER_HORIZONTAL);
                                toast.show();
                                et_comment.setText("");

                            } else {
                                Toast.makeText(YoutubePlayer.this, getString(R.string.error_comment), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.e("exception 1", e.toString());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(YoutubePlayer.this, getString(R.string.error_comment), Toast.LENGTH_SHORT).show();
                        Log.e("exception 1", error.toString());

                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        } catch (Exception e) {
            Log.e("exception 1", e.toString());
            e.printStackTrace();
        }
    }

    public void addVideoViews() {
        final String TAG = "your add View";
        HashMap<String, String> params = new HashMap<>();
        params.put("id", id);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(URL_VIDEO_VIEWS, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

}
