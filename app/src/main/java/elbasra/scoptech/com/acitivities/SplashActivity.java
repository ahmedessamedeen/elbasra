package elbasra.scoptech.com.acitivities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.helpers.CheckInternet;

public class SplashActivity extends AppCompatActivity {

    Animation animnormal, animend,animstat;
    ImageView iv;
    private static int SPLASH_TIME_OUT =4000;
    private String URL_VER = "http://basra.magdsoft.com/get_version/2.3";

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);

    }

    /**
     * Called when the activity is first created.
     */
    Thread splashTread;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        StartAnimations();
    }

    private void StartAnimations() {
        iv = (ImageView) findViewById(R.id.splashimage);
        animnormal =AnimationUtils.loadAnimation(this, R.anim.normal);
//        animstat= AnimationUtils.loadAnimation(this, R.anim.alphastart);
        animstat= AnimationUtils.loadAnimation(this, R.anim.translate);
        animend= AnimationUtils.loadAnimation(this, R.anim.alphaend);


       /* iv.startAnimation(animend);
        iv.clearAnimation();*/
        AnimationSet s = new AnimationSet(true);//false means don't share interpolators
//        s.addAnimation(animstat);
        s.addAnimation(animnormal);
//        s.addAnimation(animend);
        iv.startAnimation(s);
//        anim.reset();
        if(CheckInternet.internetConnectionAvailable(3000)){
            VersionVolley(URL_VER);
        }else {
            Dialog("" , 2);
        }


    }

    public void Dialog(String Time , final int num){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                SplashActivity.this);

        if(num == 0){
           Time = getString(R.string.new_version) + Time;
        }else if(num == 1) {
            Time = getString(R.string.old_version);
        }else {
            Time = getString(R.string.no_internet_connection);
        }

        // set title
        if(num == 0 | num == 1){
            alertDialogBuilder.setTitle(getString(R.string.version));
        }else {
            alertDialogBuilder.setTitle(getString(R.string.no_internet));
        }


        // set dialog message
        alertDialogBuilder
                .setMessage(Time)
                .setCancelable(false)
                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        if(num == 0){
                            dialog.dismiss();
                            SplashRun();
                        }else if(num == 1) {
                            finish();
                        }else {
                            recreate();
                        }

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void SplashRun(){
        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < SPLASH_TIME_OUT) {
                        sleep(100);
                        waited += 100;
                    }

                    Intent intent = new Intent(SplashActivity.this,
                            Welcome.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    SplashActivity.this.finish();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    SplashActivity.this.finish();
                }

            }
        };
        splashTread.start();
    }

    public void VersionVolley(String url){
        final String TAG = "Version URL :";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e(TAG , response.toString());
                try {
                    JSONObject jsonObject = response.getJSONObject(0);
                    String Status = jsonObject.getString("status");
                    if(Status.equals("Latest")){
                        SplashRun();
                    }else if(Status.equals("Soon")){
                        Dialog(jsonObject.getString("ExpireDate") , 0);
                    }else{
                        // Expired
                        Dialog("" , 1);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}
