package elbasra.scoptech.com.acitivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ActionMenuView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import elbasra.scoptech.com.R;
import elbasra.scoptech.com.adapters.NavListAdapter;
import elbasra.scoptech.com.adapters.RecUploadImages;
import elbasra.scoptech.com.helpers.AppController;
import elbasra.scoptech.com.helpers.CheckInternet;
import elbasra.scoptech.com.interfaces.OnStartDragListener;
import elbasra.scoptech.com.helpers.SimpleItemTouchHelperCallback;
import elbasra.scoptech.com.models.Video;
import elbasra.scoptech.com.models.getData;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SendNews extends AppCompatActivity implements UploadStatusDelegate, OnStartDragListener {
    private static final int REQUEST_WRITE_PERMISSION = 786;
    private static int RESULT_LOAD_IMAGE = 1;
    private ProgressDialog dialog;
    String uploadFilePath = "";
    List<getData> listUpload = new ArrayList<getData>();
    RecyclerView rv_upload_images;
    public static final String path = null;
    public static ArrayList<String> image_path ;
    HashMap<String, String> params;
    RecUploadImages adAdapter;
    private ItemTouchHelper mItemTouchHelper;
    DrawerLayout drawerLayout;
    Button btn_sendNews;
    AutoCompleteTextView user_info,title,content;
    Button send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_news);
        image_path = new ArrayList<>();
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage("جاري التحميل");

        content = (AutoCompleteTextView) findViewById(R.id.content);
        title = (AutoCompleteTextView) findViewById(R.id.title);
        user_info = (AutoCompleteTextView) findViewById(R.id.user_info);
        send = (Button) findViewById(R.id.btn_sendNews);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attempSend();
            }
        });

        btn_sendNews = (Button) findViewById(R.id.btn_sendNews);
        btn_sendNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckInternet.internetConnectionAvailable(3000)){
                    attempSend();
                }else{
                    Toast.makeText(SendNews.this, "لا يوجد إتصال بالإنترنت", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rv_upload_images = (RecyclerView) findViewById(R.id.rv_upload_images);
        StaggeredGridLayoutManager staggeredGridLayoutManager =new StaggeredGridLayoutManager(1 , StaggeredGridLayoutManager.HORIZONTAL);
        staggeredGridLayoutManager.setReverseLayout(true);
        rv_upload_images.setLayoutManager(staggeredGridLayoutManager);

//        layoutManager.setReverseLayout(true);
//        rv_upload_images.setLayoutManager(layoutManager);
        adAdapter = new RecUploadImages(SendNews.this, listUpload);
        rv_upload_images.setAdapter(adAdapter);
        adAdapter.notifyDataSetChanged();
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(rv_upload_images);
        NavAdapter();
    }

    public void attempSend(){
        boolean cancel = false;
        View focusView = null;
        content.setError(null);
        user_info.setError(null);
        title.setError(null);
        if(CheckInternet.internetConnectionAvailable(3000)){
            if (TextUtils.isEmpty(user_info.getText().toString())) {
                user_info.setError(getString(R.string.error_field_required));
                focusView = user_info;
                cancel = true;
            }
            if (TextUtils.isEmpty(title.getText().toString())) {
                title.setError(getString(R.string.error_field_required));
                focusView = title;
                cancel = true;
            }
            if (TextUtils.isEmpty(content.getText().toString())) {
                content.setError(getString(R.string.error_field_required));
                focusView = content;
                cancel = true;
            }else {
                if (!uploadFilePath.equals("")) {
                    sendNews("http://basra.magdsoft.com/sendContent");
                } else {
                    Toast.makeText(this, getString(R.string.image_field_empty), Toast.LENGTH_SHORT).show();
                }
            }

        }
        else{
            Toast.makeText(SendNews.this, ""+R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            uploadFilePath = cursor.getString(columnIndex);

            image_path.add(uploadFilePath);
            Log.e("file name : ", "" + uploadFilePath);
            Bitmap bitmap = null;

            try {
                // use this line to convert the path of iamge fron url to bitamp to show it in the image
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            bitmap = Bitmap.createScaledBitmap(bitmap, 256, 256, true);
            ImageView imageLoader = new ImageView(SendNews.this);
            LinearLayout.LayoutParams layoutParams = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                layoutParams = new ActionMenuView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                imageLoader.setLayoutParams(layoutParams);

            }

            imageLoader.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageLoader.setAdjustViewBounds(true);
            imageLoader.setImageBitmap(bitmap);

            listUpload.add(new getData(imageLoader));
            adAdapter.notifyDataSetChanged();
            /*ImageView iv_servantImageView = (ImageView) findViewById(R.id.iv_image_upload);
            iv_servantImageView.setImageBitmap(bitmap);*/


            cursor.close();
        }
    }

    @Override
    public void onProgress(UploadInfo uploadInfo) {

    }

    @Override
    public void onError(UploadInfo uploadInfo, Exception exception) {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
        image_path.clear();
        Toast.makeText(this, "تم", Toast.LENGTH_SHORT).show();
        ByteArrayInputStream bis = new ByteArrayInputStream(serverResponse.getBody());
        Scanner scn = new Scanner(bis);
        while(scn.hasNext()) {
            Log.i("line", scn.nextLine());
        }

//        Log.e("your Respone is :", serverResponse.getBodyAsString());

        dialog.dismiss();
        finish();
    }

    @Override
    public void onCancelled(UploadInfo uploadInfo) {
        Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void back(View view) {
        finish();
    }

    public void UploadImage(View view) {
        requestPermission();
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
    }
    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
    private void NavAdapter(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final LinearLayout drawerPanel = (LinearLayout) findViewById(R.id.drawer_panel);
        final ListView lvNav = (ListView) findViewById(R.id.nav_list);
        final List<getData> navList = new ArrayList<>();

        navList.add(new getData(getString(R.string.news), R.drawable.sm1));
        navList.add(new getData(getString(R.string.ads), R.drawable.sm2));
        navList.add(new getData(getString(R.string.videos), R.drawable.sm3));
        navList.add(new getData(getString(R.string.sport), R.drawable.sm4));
        navList.add(new getData(getString(R.string.various), R.drawable.sm5));
        navList.add(new getData(getString(R.string.send_news), R.drawable.sm6));
        navList.add(new getData(getString(R.string.contact_us), R.drawable.sm7));
        navList.add(new getData(getString(R.string.share) , R.drawable.bsra_share_app));

        NavListAdapter navListAdapter = new NavListAdapter(this , R.layout.custom_nav_list_item , navList);
        lvNav.setAdapter(navListAdapter);
        navListAdapter.notifyDataSetChanged();

        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goNews = new Intent(SendNews.this,News.class);
                switch (position){
                    case 0:
                        goNews.putExtra("type" , 1);
                        startActivity(goNews);
                        break;
                    case 1:
                        goNews.putExtra("type" , 2);
                        startActivity(goNews);
                        break;
                    case 2:
                        startActivity(new Intent(SendNews.this,YoutubeVideos.class).putExtra("search_text",""));
                        break;
                    case 3:
                        goNews.putExtra("type" , 3);
                        startActivity(goNews);
                        break;
                    case 4:
                        goNews.putExtra("type" , 4);
                        startActivity(goNews);
                        break;
                    case 5:
                        startActivity(new Intent(SendNews.this,SendNews.class));
                        break;
                    case 6:
                        startActivity(new Intent(SendNews.this,ContactUs.class));
                        break;
                    case 7:
                        Intent share = new Intent();
                        share.setAction(Intent.ACTION_SEND);
                        share.putExtra(Intent.EXTRA_TEXT , "https://play.google.com/store/apps/details?id=elbasra.scoptech.com");
                        share.setType("text/plain");
                        startActivity(share);
                        break;
                }
                setTitle(navList.get(position).getNavTitle());
                lvNav.setItemChecked(0, true);
                drawerLayout.closeDrawer(drawerPanel);
            }
        });
    }
    public void menu(View v){
        drawerLayout.openDrawer(Gravity.RIGHT);
    }

    public void getVideosVolley(String url){
        final String TAG = "your Data Return is :";
//        final HashMap<String, String> params = new HashMap<>();
//        params.put("name", );
//        params.put("link", "1");
        Log.e("params is  :", params.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url,new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG , response.toString());
                try {
                    String status = response.getString("Status");
                    JSONArray dataArray = response.getJSONArray("Data");
                    if(status.equals("Done")) {
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                        }


                    }else{
                        Toast.makeText(SendNews.this, "Error", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(AddAdvert.this, error.getMessage(), Toast.LENGTH_LONG).show();
                dialog.dismiss();
                if (error instanceof NoConnectionError) {
                    Log.d("NoConnectionError>>>>", "NoConnectionError.......");
                } else if (error instanceof AuthFailureError) {
                    Log.d("AuthFailureError>>>>", "AuthFailureError.......");
                } else if (error instanceof ServerError) {
                    Log.d("ServerError>>>>>>>>>", "ServerError.......");
                } else if (error instanceof NetworkError) {
                    Log.d("NetworkError>>>>>>>>>", "NetworkError.......");
                } else if (error instanceof ParseError) {
                    Log.d("ParseError>>>>>>>>>", "ParseError.......");
                } else if (error instanceof TimeoutError) {
                    Log.d("TimeoutError>>>>>>>>>", "TimeoutError.......");
                }
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void sendNews(String url) {
        final String TAG = "your Response is : ";
        dialog.show();
        MultipartUploadRequest request = new MultipartUploadRequest(SendNews.this, url).setUtf8Charset();
        String[] all_image = new String[image_path.size()];
        all_image = image_path.toArray(all_image);
        for (String path : all_image) {
            try {
                request.addFileToUpload(path, "uploadedfile[]").setNotificationConfig(new UploadNotificationConfig());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {
            request.addParameter("name", title.getText().toString())
                    .addParameter("user_data", user_info.getText().toString())
                    .addParameter("content", content.getText().toString())
                    .setDelegate(SendNews.this)
                    .setMaxRetries(3)
                    .startUpload();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
